# Mein-VokabeltrainerBackend
## Description
*Description follows soon*

## Endpointinformation

[Link to Endpointdocumentation](https://drive.google.com/file/d/1iGKBncrvyglErx59Oj3ifBGjfo8poAxQ/view?usp=sharing)

## How to use / test / (Install Guid)

1. Download [IntelliJ](https://www.jetbrains.com/idea/download/#section=windows) and install it 
2. Download [MySQL Installer for Windows](https://dev.mysql.com/downloads/) and install it
    1. Follow *all* the setup instructions and remember your username and password
    2. After the installation create a database with the name mein-vokabeltrainer-db
3. Clone this repository and open the project in IntelliJ
    1. In the file application.properties (src/main/resources/application.properties) change the spring.datasource.username and the spring.datasource.password to your username and password from 2.1
4. Start the spring application (VocableTrainerApplication src/main/java/de/mein/vokabeltrainer/backend/VocableTrainerApplication.java)
5. Open [Swagger](http://localhost:8080/swagger-ui.html#/)

## Others

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).
