package de.meinVokabeltrainer.backend.data.model;


import de.meinVokabeltrainer.backend.data.testsuppliers.TestSuppliers;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

class ComparingTest {

    @Test
    public void isUserInputCorrect_EqualCharacters_True() {
        Vocable testVocable = TestSuppliers.getVocableSupplierWith10PercentErrorInputAndRandomInputAmount().get();
        testVocable.setVocableGoalLanguage("Testübersätzung");
        String testUserInput = ("Testübersätzung");

        Comparing comparing = new Comparing(testUserInput, testVocable);

        assertTrue(comparing.isCorrect());
    }

    @Test
    public void isUserInputCorrect_EqualCharactersButWhiteSpacesInInput_True() {
        Vocable testVocable = TestSuppliers.getVocableSupplierWith10PercentErrorInputAndRandomInputAmount().get();
        testVocable.setVocableGoalLanguage("Testübersätzung");
        String testUserInput = ("  Testübersätzung   ");

        Comparing comparing = new Comparing(testUserInput, testVocable);

        assertTrue(comparing.isCorrect());
    }

    @Test
    public void isUserInputCorrect_EqualCharactersButDifferentCase_True() {
        Vocable testVocable = TestSuppliers.getVocableSupplierWith10PercentErrorInputAndRandomInputAmount().get();
        testVocable.setVocableGoalLanguage("Testübersätzung");
        String testUserInput = ("testübersätZung");

        Comparing comparing = new Comparing(testUserInput, testVocable);

        assertTrue(comparing.isCorrect());
    }

    @Test
    public void isUserInputCorrect_NotEqualCharacters_False() {
        Vocable testVocable = TestSuppliers.getVocableSupplierWith10PercentErrorInputAndRandomInputAmount().get();
        testVocable.setVocableGoalLanguage("Testübersätzung");
        String testUserInput = ("testübersätyung");

        Comparing comparing = new Comparing(testUserInput, testVocable);

        assertFalse(comparing.isCorrect());
    }

    @Test
    public void isMessageCorrectSet_CorrectMessageSet_IsEqual() {
        Vocable testVocable = TestSuppliers.getVocableSupplierWith10PercentErrorInputAndRandomInputAmount().get();
        testVocable.setVocableGoalLanguage("Testübersätzung");
        String testUserInput = ("Testübersätzung");

        Comparing comparing = new Comparing(testUserInput, testVocable);

        assertEquals(Comparing.CORRECT_INPUT_MESSAGE, comparing.getMessage());
    }

    @Test
    public void isMessageCorrectSet_FalseMessageSet_IsEqual() {
        Vocable testVocable = TestSuppliers.getVocableSupplierWith10PercentErrorInputAndRandomInputAmount().get();
        testVocable.setVocableGoalLanguage("Testübersätzung");
        String testUserInput = ("Testübersätyung");

        Comparing comparing = new Comparing(testUserInput, testVocable);

        assertEquals(Comparing.WRONG_INPUT_MESSAGE, comparing.getMessage());
    }
}