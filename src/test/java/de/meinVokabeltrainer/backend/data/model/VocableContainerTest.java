package de.meinVokabeltrainer.backend.data.model;

import de.meinVokabeltrainer.backend.data.testsuppliers.TestSuppliers;
import org.junit.Test;

import static org.junit.Assert.*;

public class VocableContainerTest {
    @Test
    public void equalContainer_equalValuesExceptId_EqualIsTrue(){
        VocableContainer originalVocableContainer = TestSuppliers.getVocableContainerSupplierWithDefaultValues().get();
        VocableContainer vocableContainerWhichIsEqual = TestSuppliers.getVocableContainerSupplierWithDefaultValues().get();

        assertEquals(originalVocableContainer, vocableContainerWhichIsEqual);
    }

    @Test
    public void hashVocableContainer_generateHash_getHash(){
        VocableContainer vocableContainer = TestSuppliers.getVocableContainerSupplierWithDefaultValues().get();

        assertEquals(6, vocableContainer.generateShareableKey().length());
    }
}