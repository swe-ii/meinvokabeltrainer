package de.meinVokabeltrainer.backend.data.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class VocableTest {
    @Test
    public void createVocable_NullCheck_UsageOfDefaultValues(){
        Vocable resultVocable = Vocable.builder()
                .addValueBaseLanguage(null)
                .addValueGoalLanguage(null)
                .defineWordClass(null);

        Vocable expectedVocable = Vocable.builder()
                .addValueBaseLanguage("not defined")
                .addValueGoalLanguage("not defined")
                .defineWordClass("not defined");

        assertEquals(expectedVocable.toString(), resultVocable.toString());
    }

    @Test
    public void equalMethod_SameValues_equalsTrue(){
        Vocable expectedVocable = Vocable.builder()
                .addValueBaseLanguage("Hallo")
                .addValueGoalLanguage("Hello")
                .defineWordClass("Neutrum");

        expectedVocable.setVocableId(1L);

        Vocable resultVocable = Vocable.builder()
                .addValueGoalLanguage("Hello")
                .defineWordClass("Neutrum")
                .addValueBaseLanguage("Hallo");

        resultVocable.setVocableId(2L);

        assertEquals(expectedVocable, resultVocable);
    }

    @Test
    public void equalMethod_SameBaseValuesDifferentValuesAfterSimulatedUsage_equalsFalse(){
        Vocable expectedVocable = Vocable.builder()
                .addValueBaseLanguage("Hallo")
                .addValueGoalLanguage("Hello")
                .defineWordClass("Neutrum");

        expectedVocable.setIncorrectInputAmount(20);
        expectedVocable.setTotalInputAmount(12);

        Vocable resultVocable = Vocable.builder()
                .addValueGoalLanguage("Hello")
                .defineWordClass("Neutrum")
                .addValueBaseLanguage("Hallo");

        resultVocable.setTotalInputAmount(1);
        resultVocable.setIncorrectInputAmount(0);

        assertNotEquals(expectedVocable, resultVocable);
    }
}