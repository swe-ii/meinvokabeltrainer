package de.meinVokabeltrainer.backend.data.testsuppliers;

import de.meinVokabeltrainer.backend.data.model.Vocable;
import de.meinVokabeltrainer.backend.data.model.VocableContainer;

import java.util.Random;
import java.util.function.Supplier;

public class TestSuppliers {
    static Supplier<Vocable> vocableSupplierWith50PercentErrorInputAndRandomInputAmount = () -> {
        Vocable vocable = Vocable.builder()
                .addValueBaseLanguage("temp")
                .addValueGoalLanguage("temp")
                .defineWordClass("Nomen");

        int incorrectInputAmount = new Random().nextInt(1000) + 1;

        vocable.setIncorrectInputAmount(incorrectInputAmount);
        vocable.setTotalInputAmount(incorrectInputAmount * 2);

        return vocable;
    };

    static Supplier<Vocable> vocableSupplierWith10PercentErrorInputAndRandomInputAmount = () -> {
        Vocable vocable = Vocable.builder()
                .addValueBaseLanguage("temp")
                .addValueGoalLanguage("temp")
                .defineWordClass("Nomen");

        int incorrectInputAmount = new Random().nextInt(1000) + 1;

        vocable.setIncorrectInputAmount(incorrectInputAmount);
        vocable.setTotalInputAmount(incorrectInputAmount * 10);

        return vocable;
    };

    static Supplier<VocableContainer> vocableContainerSupplierWithDefaultValues = () -> {
        VocableContainer vocableContainer = new VocableContainer();

        vocableContainer.setTitle("not defined");
        vocableContainer.setShareableKey("not yet implemented");
        vocableContainer.setVocableContainerId("1");
        vocableContainer.setGoalLanguage("temp");
        vocableContainer.setBaseLanguage("temp");

        return vocableContainer;
    };

    public static Supplier<Vocable> getVocableSupplierWith50PercentErrorInputAndRandomInputAmount() {
        return vocableSupplierWith50PercentErrorInputAndRandomInputAmount;
    }

    public static Supplier<Vocable> getVocableSupplierWith10PercentErrorInputAndRandomInputAmount() {
        return vocableSupplierWith10PercentErrorInputAndRandomInputAmount;
    }

    public static Supplier<VocableContainer> getVocableContainerSupplierWithDefaultValues() {
        return vocableContainerSupplierWithDefaultValues;
    }
}
