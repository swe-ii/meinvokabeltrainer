package de.meinVokabeltrainer.backend.data.service;

import de.meinVokabeltrainer.backend.data.model.Vocable;
import org.junit.Test;

import static org.junit.Assert.*;

public class VocableServiceTest {
    @Test
    public void calculatePercentage_10Correct20Wrong_66PercentResult(){
        Vocable vocable = Vocable.builder()
                .addValueBaseLanguage("Apfel")
                .addValueGoalLanguage("Apple")
                .defineWordClass("Nomen");

        vocable.setTotalInputAmount(30);
        vocable.setIncorrectInputAmount(20);

        VocableService vocableService = new VocableService();

        assertEquals(20d/30d, vocable.getErrorPercentage(), 0.2d);
        assertEquals(10d/30d, vocableService.getCorrectPercentage(vocable), 0.2d);
    }
}