package de.meinVokabeltrainer.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class VocableTrainerApplication {
    public static String versioncode = "1.5.0";

    public static void main(String[] args) {
        SpringApplication.run(VocableTrainerApplication.class, args);
    }
}
