package de.meinVokabeltrainer.backend.restcontroller;

import de.meinVokabeltrainer.backend.data.model.Vocable;
import de.meinVokabeltrainer.backend.data.model.VocableContainer;
import de.meinVokabeltrainer.backend.data.service.VocableContainerService;
import de.meinVokabeltrainer.backend.data.service.VocableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@Api(value = "Vocable System")
public class VocableController {
    @Autowired
    VocableContainerService vocableContainerService;
    @Autowired
    VocableService vocableService;

    @ApiOperation(value = "Returns all vocables belonging to the vocableController with given errorQuote as filter (>=).",
            response = List.class)
    @GetMapping(value = "/vocable/{vocableContainerId}/{errorQuote}", produces = "application/json;charset=UTF-8")
    public List<Vocable> getAllVocablesByErrorQuote(@PathVariable(name = "vocableContainerId") String vocableContainerId,
                                                    @PathVariable(name = "errorQuote") final Double errorQuote) {
        return getAllVocablesByContainerId(vocableContainerId).stream()
                .filter(vocable -> vocable.getErrorPercentage() >= errorQuote)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Returns all vocables belonging to the vocableContainer", response = List.class)
    @GetMapping(value = "/vocable/{vocableContainerId}", produces = "application/json;charset=UTF-8")
    public List<Vocable> getAllVocablesByContainerId(@PathVariable(name = "vocableContainerId") String vocableContainerId) {
        return vocableService
                .getAllVocablesFromDatabaseByVocableContainer(vocableContainerService
                        .getVocableContainerFromDatabaseById(vocableContainerId));
    }

    @ApiOperation(value = "Returns a List of all vocables belonging to a vocableController which have not been trained yet",
            response = List.class)
    @GetMapping(value = "/vocable/{vocableContainerId}/notTrained", produces = "application/json;charset=UTF-8")
    public List<Vocable> getAllVocablesWhichAreNotTrained(@PathVariable(name = "vocableContainerId")
                                                                  String vocableContainerId) {
        return getAllVocablesByContainerId(vocableContainerId).stream()
                .filter(vocable -> vocable.getTotalInputAmount() == 0)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Deletes a Vocable if its in the vocableContainer")
    @DeleteMapping(value = "/vocable/{vocableContainerId}/{vocableId}", produces = "application/json;charset=UTF-8")
    public ResponseEntity deleteVocable(@PathVariable(name = "vocableContainerId") String vocableContainerId,
                                        @PathVariable(name = "vocableId") Long vocableId) {
        if (vocableService.deleteVocable(vocableId, vocableContainerId)) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "Adds a List of vocables to the vocableController", response = List.class)
    @PostMapping(value = "/vocable/{vocableContainerId}/", produces = "application/json;charset=UTF-8")
    public ResponseEntity addVocableToVocableContainer(@PathVariable(name = "vocableContainerId")
                                                                   String vocableContainerId,
                                                       @RequestBody Vocable vocableToAdd) {
        final int MAX_VOCABLE_PER_CONTAINER = 200;
        VocableContainer vocableContainerFromId = vocableContainerService.getVocableContainerFromDatabaseById(vocableContainerId);
        boolean underMaxSize = vocableService.getAllVocablesFromDatabaseByVocableContainer(vocableContainerFromId).size() <= MAX_VOCABLE_PER_CONTAINER;

        if (vocableContainerFromId != null && !checkForEmptyInput(vocableToAdd) && underMaxSize) {
            vocableToAdd.setVocableContainer(vocableContainerFromId);
            vocableToAdd = vocableService.saveVocable(vocableToAdd);

            return ResponseEntity.status(HttpStatus.OK)
                    .body(vocableToAdd);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    private boolean checkForEmptyInput(Vocable vocable) {
        if (vocable.getVocableBaseLanguage() == null) {
            return true;
        } else if (vocable.getVocableBaseLanguage().trim().isBlank()) {
            return true;
        } else if (vocable.getVocableGoalLanguage() == null) {
            return true;
        } else return vocable.getVocableGoalLanguage().trim().isBlank();
    }

    @ApiOperation(value = "Updates a Vocable if its in the vocableContainer")
    @PutMapping(value = "/vocable/{vocableContainerId}/{vocableId}", produces = "application/json;charset=UTF-8")
    public ResponseEntity updateVocable(@PathVariable(name = "vocableContainerId") String vocableContainerId,
                                        @PathVariable(name = "vocableId") Long vocableId,
                                        @RequestBody Vocable vocableWithUpdates) {
        if (vocableContainerService.isPartOfContainer(vocableContainerId, vocableId)) {
            vocableWithUpdates = vocableService.updateVocable(vocableId, vocableWithUpdates);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(vocableService.updateVocable(vocableId, vocableWithUpdates));
        }

        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .build();
    }
}
