package de.meinVokabeltrainer.backend.restcontroller;

import de.meinVokabeltrainer.backend.data.model.Comparing;
import de.meinVokabeltrainer.backend.data.model.Vocable;
import de.meinVokabeltrainer.backend.data.service.VocableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "vocable input comparing controller")
@CrossOrigin
@RestController
public class ComparingController {
    @Autowired
    private VocableService vocableService;

    @ApiOperation(value = "Compares the userInput to the saved value and updates the vocable in the database")
    @PostMapping(value = "/vocableComparing/{vocableId}/{userInput}", produces = "application/json;charset=UTF-8")
    public Comparing checkIfUserInputIsCorrect(@PathVariable(name = "vocableId") Long vocableId,
                                               @PathVariable(name = "userInput") String userInput) {
        Vocable trainingVocable = vocableService.getVocableById(vocableId);

        if (trainingVocable == null) {
            return null;
        }

        trainingVocable.increaseTotalInputAmount();
        Comparing comparingResult = new Comparing(userInput, trainingVocable);
        vocableService.updateVocable(vocableId, trainingVocable);

        return comparingResult;
    }
}
