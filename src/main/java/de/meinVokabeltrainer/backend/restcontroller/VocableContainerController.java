package de.meinVokabeltrainer.backend.restcontroller;

import de.meinVokabeltrainer.backend.captcha.CaptchaResponse;
import de.meinVokabeltrainer.backend.captcha.CaptchaValidator;
import de.meinVokabeltrainer.backend.data.model.Vocable;
import de.meinVokabeltrainer.backend.data.model.VocableContainer;
import de.meinVokabeltrainer.backend.data.service.VocableContainerService;
import de.meinVokabeltrainer.backend.data.service.VocableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "VocableContainer System")
@CrossOrigin
@RestController
public class VocableContainerController {
    public final int MAX_VOCABLECONTAINER_AMOUNT = 20000;
    @Autowired
    private VocableContainerService vocableContainerService;
    @Autowired
    private VocableService vocableService;

    @Autowired
    private CaptchaValidator captchaValidator;

    @ApiOperation(value = "Get a vocableContainer by its id")
    @GetMapping(value = "/vocableContainers/{vocableContainerId}", produces = "application/json;charset=UTF-8")
    public VocableContainer getVocableContainerById(@PathVariable(name = "vocableContainerId") String vocableContainerId) {
        return vocableContainerService.getVocableContainerFromDatabaseById(vocableContainerId);
    }

    @ApiOperation(value = "Get a vocableContainer by its shareable key")
    @GetMapping(value = "/vocableContainers/byKey/{shareableKey}", produces = "application/json;charset=UTF-8")
    public VocableContainer getVocableContainerByShareableKey(@PathVariable(name = "shareableKey") String shareableKey) {
        VocableContainer resultContainer = vocableContainerService.getVocableContainerFromDatabaseByShareableKey(shareableKey);

        if (resultContainer == null){
            return null;
        }

        if (resultContainer.getShareableKey().equals(shareableKey)){
            return resultContainer;
        }

        return null;
    }

    @ApiOperation(value = "Add a vocableContainers (without vocables)")
    @PostMapping(value = "/vocableContainers", produces = "application/json;charset=UTF-8")
    public ResponseEntity postVocableContainer(@RequestHeader("recaptcha") String captchaResponse,
                                               @RequestBody VocableContainer vocableContainerToAdd) {
        Boolean isValidCaptcha = captchaValidator.validateCaptcha(captchaResponse);
        if(!isValidCaptcha){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        if (checkForEmptyInput(vocableContainerToAdd)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body(vocableContainerService.saveVocableContainerToDatabase(vocableContainerToAdd));
    }

    private boolean checkForEmptyInput(VocableContainer containerToCheck) {
        if (containerToCheck.getBaseLanguage() == null || containerToCheck.getGoalLanguage() == null || containerToCheck.getTitle() == null) {
            return true;
        }

        return containerToCheck.getBaseLanguage().trim().isBlank() || containerToCheck.getGoalLanguage().trim().isBlank() || containerToCheck.getTitle().trim().isBlank();
    }

    @ApiOperation(value = "Add a vocableContainers (with vocables) by copying creating a copy from another vocableContainer")
    @PostMapping(value = "/vocableContainers/copyContainer/{vocableContainerId}", produces = "application/json;charset=UTF-8")
    public VocableContainer createVocableContainerFromOtherContainer(@PathVariable(name = "vocableContainerId")
                                                                                 String vocableContainerId) {
        VocableContainer vocableContainerToCopy = vocableContainerService.getVocableContainerFromDatabaseById(vocableContainerId);

        if (vocableContainerToCopy == null) {
            return null;
        }

        VocableContainer copiedContainer = vocableContainerToCopy.createCopy();
        copiedContainer = vocableContainerService.saveVocableContainerToDatabase(copiedContainer);
        createCopyOfVocableList(vocableContainerToCopy, copiedContainer);

        return copiedContainer;
    }

    private void createCopyOfVocableList(VocableContainer vocableContainerToCopy, VocableContainer vocableContainerToCreate){
        vocableService.getAllVocablesFromDatabaseByVocableContainer(vocableContainerToCopy).stream()
                .map(Vocable::createCopy)
                .forEach(vocable -> {
                    vocable.setVocableContainer(vocableContainerToCreate);
                    vocableService.saveVocable(vocable);
                });
    }

    @ApiOperation(value = "Delete a vocableContainers (with vocables)")
    @DeleteMapping(value = "/vocableContainers/{vocableContainerId}", produces = "application/json;charset=UTF-8")
    public void deleteVocableContainer(@PathVariable(name = "vocableContainerId") String vocableContainerId) {
        VocableContainer vocableContainerToDelete = vocableContainerService
                .getVocableContainerFromDatabaseById(vocableContainerId);

        vocableContainerService.deleteVocableContainerAndItsVocables(vocableContainerToDelete);
    }

    @ApiOperation(value = "Updates a vocableContainers (without vocables)")
    @PutMapping(value = "/vocableContainers/{vocableContainerId}", produces = "application/json;charset=UTF-8")
    public ResponseEntity updateVocableContainer(@RequestBody VocableContainer vocableContainerWithUpdates,
                                                 @PathVariable(name = "vocableContainerId") String vocableContainerId) {
        VocableContainer responseVocableContainer = vocableContainerService.updateVocableContainer(vocableContainerId,
                vocableContainerWithUpdates);

        if (responseVocableContainer == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .header("The provided id doesnt belong to any known container")
                    .build();
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body(responseVocableContainer);
    }
}
