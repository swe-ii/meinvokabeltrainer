package de.meinVokabeltrainer.backend.data.model;

public class Comparing {
    public static final String CORRECT_INPUT_MESSAGE = "Deine Eingabe war richtig";
    public static final String WRONG_INPUT_MESSAGE = "Deine Eingabe war falsch";

    private Boolean correct;
    private String message, reason;

    public Comparing(String userInput, Vocable trainingVocable) {
        if (isUserInputCorrect(userInput, trainingVocable.getVocableGoalLanguage())) {
            this.correct = true;
            this.message = CORRECT_INPUT_MESSAGE;
        } else {
            trainingVocable.increaseIncorrectInputAmount();
            this.correct = false;
            this.message = WRONG_INPUT_MESSAGE;
        }

        this.reason = null;
    }

    public boolean isCorrect() {
        return correct;
    }

    public String getMessage() {
        return message;
    }

    private boolean isUserInputCorrect(String correctTranslation, String userInput) {
        return correctTranslation.trim().toLowerCase().equals(userInput.trim().toLowerCase());
    }
}
