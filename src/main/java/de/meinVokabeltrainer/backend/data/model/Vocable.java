package de.meinVokabeltrainer.backend.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Objects;
import java.util.Optional;

@ApiModel(description = "All details about the Vocable.")
@Entity
@Table(name = "vocable")
public class Vocable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated vocableId", allowEmptyValue = true)
    private Long vocableId;
    @ApiModelProperty(notes = "A string defining the word-class. Their are only 3 possible strings",
            required = true,
            allowableValues = "noun, verb, adjective",
            example = "noun")
    private String wordClass;
    @ApiModelProperty(name = "A string defining the vocable in the users known language", required = true, example = "Hallo")
    private String vocableBaseLanguage;
    @ApiModelProperty(name = "A string defining the vocable in the language the user is learning", required = true, example = "hello")
    private String vocableGoalLanguage;
    @ApiModelProperty(name = "A integer defining the total amount of training-calls. (Handled by the backend with request)"
            , allowEmptyValue = true)
    private Integer totalInputAmount;
    @ApiModelProperty(name = "A integer defining the total amount of training-calls which have been incorrectly solved. (Handled by the backend with request)"
            , allowEmptyValue = true)
    private Integer incorrectInputAmount;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "vocable_container_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private VocableContainer vocableContainer;

    protected Vocable() {
    }

    public static Vocable builder() {
        return new Vocable();
    }

    public Vocable addValueBaseLanguage(String valueBaseLanguage) {
        Optional<String> optionalOfValueBaseLanguageString = Optional.ofNullable(valueBaseLanguage);
        optionalOfValueBaseLanguageString.ifPresentOrElse(this::setVocableBaseLanguage, () -> this.setVocableBaseLanguage("not defined"));

        return this;
    }

    public Vocable addValueGoalLanguage(String valueGoalLanguage) {
        Optional<String> optionalOfValueGoalLanguageString = Optional.ofNullable(valueGoalLanguage);
        optionalOfValueGoalLanguageString.ifPresentOrElse(this::setVocableGoalLanguage, () -> this.setVocableGoalLanguage("not defined"));

        return this;
    }

    public Vocable defineWordClass(String wordClass) {
        Optional<String> optionalOfWordClassString = Optional.ofNullable(wordClass);
        optionalOfWordClassString.ifPresentOrElse(this::setWordClass, () -> this.setWordClass("not defined"));

        optionalOfWordClassString.ifPresent(this::setWordClass);

        return this;
    }

    public Long getVocableId() {
        return vocableId;
    }

    public void setVocableId(Long vocableId) {
        this.vocableId = vocableId;
    }

    public String getVocableBaseLanguage() {
        return vocableBaseLanguage;
    }

    public void setVocableBaseLanguage(String valueBaseLanguage) {
        this.vocableBaseLanguage = valueBaseLanguage;
    }

    public String getVocableGoalLanguage() {
        return vocableGoalLanguage;
    }

    public void setVocableGoalLanguage(String valueGoalLanguage) {
        this.vocableGoalLanguage = valueGoalLanguage;
    }

    public Integer getIncorrectInputAmount() {
        return incorrectInputAmount;
    }

    public void setIncorrectInputAmount(Integer incorrectInputAmount) {
        this.incorrectInputAmount = incorrectInputAmount;
    }

    public Integer getTotalInputAmount() {
        return totalInputAmount;
    }

    public void setTotalInputAmount(Integer totalInputAmount) {
        this.totalInputAmount = totalInputAmount;
    }

    public String getWordClass() {
        return wordClass;
    }

    public void setWordClass(String wordClass) {
        this.wordClass = wordClass;
    }

    public VocableContainer getVocableContainer() {
        return vocableContainer;
    }

    public void setVocableContainer(VocableContainer vocableContainer) {
        this.vocableContainer = vocableContainer;
    }

    public void increaseTotalInputAmount() {
        this.setTotalInputAmount(this.getTotalInputAmount() + 1);
    }

    public void increaseIncorrectInputAmount() {
        this.setIncorrectInputAmount(this.getIncorrectInputAmount() + 1);
    }

    @JsonIgnore
    public Double getErrorPercentage() {
        return (double) this.getIncorrectInputAmount() / (double) this.getTotalInputAmount();
    }

    @Override
    public int hashCode() {
        return Objects.hash(vocableId, vocableBaseLanguage, vocableGoalLanguage, incorrectInputAmount, totalInputAmount, wordClass);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vocable vocable = (Vocable) o;

        return Objects.equals(vocableBaseLanguage, vocable.vocableBaseLanguage) &&
                Objects.equals(vocableGoalLanguage, vocable.vocableGoalLanguage) &&
                Objects.equals(incorrectInputAmount, vocable.incorrectInputAmount) &&
                Objects.equals(totalInputAmount, vocable.totalInputAmount) &&
                Objects.equals(wordClass, vocable.wordClass);
    }

    public Vocable createCopy() {
        Vocable copy = new Vocable();

        copy.setWordClass(this.wordClass);
        copy.setVocableGoalLanguage(this.vocableGoalLanguage);
        copy.setVocableBaseLanguage(this.vocableBaseLanguage);
        copy.setTotalInputAmount(0);
        copy.setIncorrectInputAmount(0);

        return copy;
    }

    @Override
    public String toString() {
        return "Vocable{" +
                "vocableId=" + vocableId +
                ", valueBaseLanguage='" + vocableBaseLanguage + '\'' +
                ", valueGoalLanguage='" + vocableGoalLanguage + '\'' +
                ", incorrectInputAmount=" + incorrectInputAmount +
                ", totalInputAmount=" + totalInputAmount +
                ", wordClass='" + wordClass + '\'' +
                '}';
    }
}