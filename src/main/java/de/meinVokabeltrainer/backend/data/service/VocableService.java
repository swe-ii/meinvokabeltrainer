package de.meinVokabeltrainer.backend.data.service;

import de.meinVokabeltrainer.backend.data.model.Vocable;
import de.meinVokabeltrainer.backend.data.model.VocableContainer;
import de.meinVokabeltrainer.backend.data.repository.VocableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VocableService {
    @Autowired
    private VocableRepository vocableRepository;

    public Vocable saveVocable(Vocable vocableToSave) {
        this.checkInputAndIncorrectInputForNull(vocableToSave);
        return vocableRepository.save(vocableToSave);
    }

    private void checkInputAndIncorrectInputForNull(Vocable vocableToCheck) {
        if (vocableToCheck.getTotalInputAmount() == null) {
            vocableToCheck.setTotalInputAmount(0);
        }

        if (vocableToCheck.getIncorrectInputAmount() == null) {
            vocableToCheck.setIncorrectInputAmount(0);
        }
    }

    public boolean deleteVocable(Long vocableId, String vocableContainerId) {
        Vocable vocableToDelete = getVocableById(vocableId);

        if (vocableToDelete == null){
            return false;
        }

        if (vocableToDelete.getVocableContainer().getVocableContainerId().equals(vocableContainerId)) {
            vocableRepository.deleteById(vocableId);
        }else{
            return false;
        }

        return true;
    }

    public Vocable updateVocable(Long vocableId, Vocable vocableWithUpdates) {
        Vocable vocableToUpdate = vocableRepository.findByVocableId(vocableId);

        if (vocableToUpdate == null) {
            return null;
        }

        if (vocableWithUpdates.getIncorrectInputAmount() != null) {
            vocableToUpdate.setIncorrectInputAmount(vocableWithUpdates.getIncorrectInputAmount());
        }

        if (vocableWithUpdates.getTotalInputAmount() != null) {
            vocableToUpdate.setTotalInputAmount(vocableWithUpdates.getTotalInputAmount());
        }

        if (vocableWithUpdates.getVocableBaseLanguage() != null) {
            vocableToUpdate.setVocableBaseLanguage(vocableWithUpdates.getVocableBaseLanguage());
        }

        if (vocableWithUpdates.getVocableGoalLanguage() != null) {
            vocableToUpdate.setVocableGoalLanguage(vocableWithUpdates.getVocableGoalLanguage());
        }

        if (vocableWithUpdates.getWordClass() != null) {
            vocableToUpdate.setWordClass(vocableWithUpdates.getWordClass());
        }

        return vocableRepository.save(vocableToUpdate);
    }

    public List<Vocable> getAllVocablesFromDatabaseByVocableContainer(VocableContainer vocableContainer) {
        return vocableRepository.findAllByVocableContainer(vocableContainer);
    }

    public Vocable getVocableById(Long vocableId) {
        return vocableRepository.findByVocableId(vocableId);
    }

    public Double getCorrectPercentage(Vocable vocable) {
        return 1d - vocable.getErrorPercentage();
    }
}
