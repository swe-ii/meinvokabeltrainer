package de.meinVokabeltrainer.backend.data.service;

import de.meinVokabeltrainer.backend.data.model.Vocable;
import de.meinVokabeltrainer.backend.data.model.VocableContainer;
import de.meinVokabeltrainer.backend.data.repository.VocableContainerRepository;
import de.meinVokabeltrainer.backend.data.repository.VocableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VocableContainerService {
    @Autowired
    VocableRepository vocableRepository;
    @Autowired
    private VocableContainerRepository vocableContainerRepository;
    @Autowired
    private VocableService vocableService;

    public VocableContainer saveVocableContainerToDatabase(VocableContainer vocableContainerToAdd) {
        VocableContainer savedVocableContainer = vocableContainerRepository.save(vocableContainerToAdd);
        generateKeyIfNotSet(savedVocableContainer);

        return vocableContainerRepository.save(savedVocableContainer);
    }

    public void deleteVocableContainerAndItsVocables(VocableContainer vocableContainerToDelete) {
        if (vocableContainerToDelete != null) {
            vocableRepository.deleteAllByVocableContainer(vocableContainerToDelete);
            vocableContainerRepository.delete(vocableContainerToDelete);
        }
    }

    public VocableContainer getVocableContainerFromDatabaseById(String vocableContainerId) {
        return vocableContainerRepository.findByVocableContainerId(vocableContainerId);
    }

    public VocableContainer getVocableContainerFromDatabaseByShareableKey(String shareableKey) {
        return vocableContainerRepository.findByShareableKey(shareableKey);
    }

    public boolean isPartOfContainer(String vocableContainerId, Long vocableId) {
        return isPartOfContainer(getVocableContainerFromDatabaseById(vocableContainerId), vocableService.getVocableById(vocableId));
    }

    public boolean isPartOfContainer(VocableContainer vocableContainerToCheck, Vocable vocableToCheck) {
        return vocableService.getAllVocablesFromDatabaseByVocableContainer(vocableContainerToCheck).contains(vocableToCheck);
    }

    public VocableContainer updateVocableContainer(String vocableContainerId, VocableContainer vocableContainerWithUpdates) {
        VocableContainer vocableContainerToUpdate = vocableContainerRepository.findByVocableContainerId(vocableContainerId);

        updateWithNullChecking(vocableContainerToUpdate, vocableContainerWithUpdates);

        return vocableContainerRepository.save(vocableContainerToUpdate);
    }

    private void updateWithNullChecking(VocableContainer vocableContainerToUpdate, VocableContainer vocableContainerWithUpdates) {
        if (vocableContainerToUpdate == null) {
            return;
        }

        if (vocableContainerWithUpdates.getTitle() != null) {
            vocableContainerToUpdate.setTitle(vocableContainerWithUpdates.getTitle());
        }

        if (vocableContainerWithUpdates.getBaseLanguage() != null) {
            vocableContainerToUpdate.setBaseLanguage(vocableContainerWithUpdates.getBaseLanguage());
        }

        if (vocableContainerWithUpdates.getGoalLanguage() != null) {
            vocableContainerToUpdate.setGoalLanguage(vocableContainerWithUpdates.getGoalLanguage());
        }

        if (vocableContainerWithUpdates.getShareableKey() != null && !vocableContainerWithUpdates.getShareableKey().trim().isBlank()) {
            if (!checkForKeyCollision(vocableContainerWithUpdates.getShareableKey(), 1)) {
                vocableContainerToUpdate.setShareableKey(vocableContainerWithUpdates.getShareableKey());
            }
        }
    }

    public boolean checkForKeyCollision(String keyToTest, int allowedCollisions) {
        List<VocableContainer> allVocableContainers = getAllContainers();

        if (!nullCheckContainerAndString(allVocableContainers, keyToTest)) {
            return true;
        }

        long collisions = allVocableContainers.stream()
                .map(VocableContainer::getShareableKey)
                .filter(key -> {
                    if (key == null) {
                        return false;
                    }

                    return key.equals(keyToTest);
                })
                .count();

        return collisions > allowedCollisions;
    }

    public void generateKeyIfNotSet(VocableContainer vocableContainer) {
        if (vocableContainer.getShareableKey() == null || vocableContainer.getShareableKey().trim().isBlank() || checkForKeyCollision(vocableContainer.getShareableKey(), 1)) {
            do {
                vocableContainer.setShareableKey(vocableContainer.generateShareableKey());
            } while (checkForKeyCollision(vocableContainer.getShareableKey(), 1));
        }
    }

    private boolean nullCheckContainerAndString(List<VocableContainer> allVocableContainers, String keyToTest) {
        return allVocableContainers != null && keyToTest != null;
    }

    public List<VocableContainer> getAllContainers() {
        return vocableContainerRepository.findAll();
    }
}
