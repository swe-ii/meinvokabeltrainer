package de.meinVokabeltrainer.backend.data.repository;

import de.meinVokabeltrainer.backend.data.model.Vocable;
import de.meinVokabeltrainer.backend.data.model.VocableContainer;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface VocableRepository extends CrudRepository<Vocable, Long> {
    List<Vocable> findAllByVocableContainer(VocableContainer vocableContainer);

    Vocable findByVocableId(Long vocableId);

    List<Vocable> deleteAllByVocableContainer(VocableContainer vocableContainer);
}
