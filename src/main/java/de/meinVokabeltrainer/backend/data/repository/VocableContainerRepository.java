package de.meinVokabeltrainer.backend.data.repository;

import de.meinVokabeltrainer.backend.data.model.VocableContainer;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface VocableContainerRepository extends CrudRepository<VocableContainer, String> {
    VocableContainer findByVocableContainerId(String vocableContainerId);

    VocableContainer findByShareableKey(String shareableKey);

    List<VocableContainer> findAll();
}
