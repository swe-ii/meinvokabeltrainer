package de.meinVokabeltrainer.backend.captcha;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class CaptchaResponse {

    private Boolean success;
    private Date timestamp;
    private String hostname;
    @JsonProperty("error-codes")
    private List<String> errorCodes;

    public Boolean getSuccess() {
        return success;
    }

    public CaptchaResponse setSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public CaptchaResponse setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getHostname() {
        return hostname;
    }

    public CaptchaResponse setHostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public List<String> getErrorCodes() {
        return errorCodes;
    }

    public CaptchaResponse setErrorCodes(List<String> errorCodes) {
        this.errorCodes = errorCodes;
        return this;
    }
}